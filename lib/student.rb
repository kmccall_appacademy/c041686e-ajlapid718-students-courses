class Student
  attr_accessor :first_name, :last_name, :courses

	def initialize(first_name, last_name)
		@first_name = first_name
		@last_name = last_name
    @courses = []
	end

	def name
		@first_name + " " + @last_name
	end

	def enroll(new_course)
    raise "ERROR" if @courses.any? { |course| course.conflicts_with?(new_course) }
		new_course.students.push(self)
    @courses << new_course unless @courses.include?(new_course)
	end

	def course_load
		departments_to_credits = {}
    @courses.each do |course|
		    departments_to_credits[course.department] += course.credits if departments_to_credits.has_key?(course.department)
        departments_to_credits[course.department] = course.credits if !departments_to_credits.has_key?(course.department)
      end
      departments_to_credits
  end
end
